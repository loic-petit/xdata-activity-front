module.exports = function (grunt) {

    'use strict';

    require('load-grunt-tasks')(grunt);

    var config = {

        // proxified API host
        api: 'localhost',
        apiPort: 4545,

        // paths config
        tmp: '.tmp',
        src: 'src',
        dist: 'dist',

        // files config
        // the main entry point (must but loaded first)
        mainJs: 'js/xdata.js',
        // all Js files
        appJs: ['js/**/*.js'],
        // unit tests
        appSpecs: ['js/**/*.spec.js'],
        //js: ['<%= config.appJs %>', '!<%= config.mainJs %>', '!<%= config.appSpecs %>'],
        partials: ['views/**/*.tpl.html'],

        // dependencies 
        bowerPath: grunt.file.readJSON('.bowerrc').directory,
        vendorsPath: 'src/vendors',
        bowerCss: [
            '<%= config.bowerPath %>/bootstrap/dist/css/bootstrap.css',
            '<%= config.bowerPath %>/bootstrap/dist/css/bootstrap-theme.css',
            '<%= config.bowerPath %>/leaflet/dist/leaflet.css',
            '<%= config.bowerPath %>/leaflet.markercluster/dist/MarkerCluster.css',
            '<%= config.bowerPath %>/leaflet.markercluster/dist/MarkerCluster.Default.css',
            '<%= config.bowerPath %>/fontawesome/css/font-awesome.css',
            '<%= config.bowerPath %>/select2/select2.css'
        ],
        bowerJs: [
            '<%= config.bowerPath %>/jquery/dist/jquery.js',
            '<%= config.bowerPath %>/bootstrap/dist/js/bootstrap.js',
            '<%= config.bowerPath %>/angular/angular.js',
            '<%= config.bowerPath %>/angular-bindonce/bindonce.js',
            '<%= config.bowerPath %>/angular-animate/angular-animate.js',
            '<%= config.bowerPath %>/angular-ui-router/release/angular-ui-router.js',
            '<%= config.bowerPath %>/angular-bootstrap/ui-bootstrap.js',
            '<%= config.bowerPath %>/highcharts/highcharts.js',
            '<%= config.bowerPath %>/highcharts/highcharts-more.js',
            '<%= config.bowerPath %>/highcharts/modules/solid-gauge.js',
            '<%= config.bowerPath %>/highcharts-ng/dist/highcharts-ng.js',
            '<%= config.bowerPath %>/jquery-mousewheel/jquery.mousewheel.js',
            '<%= config.bowerPath %>/chroma-js/chroma.js',
            '<%= config.bowerPath %>/angular-touch/angular-touch.js',
            '<%= config.bowerPath %>/angular-slider/build/angular-slider.js'
        ],
        vendorsJs: [
        ],

        // various
        liveReloadPort: 35729
    };

    // needed for the connect middleware
    var lrSnippet = require('connect-livereload')({ port: config.liveReloadPort });
    var liveReloadEnabled = grunt.option('livereload')===true;

    grunt.initConfig({

        config: config,

        clean: {
            dist: ['<%= config.dist %>'],
            tmp: ['<%= config.tmp %>']
        },

        copy: {
            // copy files to tmp folder for the build minification
            tmp: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.src %>',
                        dest: '<%= config.tmp %>',
                        src: [
                            '*.html',
                            '*.{ico,txt}',
                            'data/**',
                            'images/**/*',
                            '<%= config.appJs %>',
                            '!<%= config.appSpecs %>'
                        ]
                    }
                ]
            },
            // copy from tmp folder to dist
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.tmp %>',
                        dest: '<%= config.dist %>',
                        src: [
                            'index.html',
                            '*.{ico,txt}',
                            'data/**',
                            'images/**/*',
                            'css/*.base.css',
                            'css/*.vendors.css',
                            'js/*.scripts.js',
                            'js/*.vendors.js'
                        ]
                    }
                ]
            },
            // copy fonts from bower
            fonts: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bowerPath %>/fontawesome',
                        dest: '<%= config.dist %>',
                        src: [
                            'fonts/*'
                        ]
                    }
                ]
            }
        },

        // css preprocessing
        less: {
            dev: {
                files: {
                    '<%= config.src %>/css/base.css': '<%= config.src %>/css/base.less'
                }
            },
            tmp: {
                options: {
                  cleancss: true,
                  compress: true
                },
                files: {
                    '<%= config.tmp %>/css/base.css': '<%= config.src %>/css/base.less'
                }
            }
        },

        // autoprefix the css
        autoprefixer: {
            options: {
                browsers: [
                    'Android >= 4',
                    'Chrome >= 20',
                    'Firefox >= 20',
                    'iOS >= 5',
                    'Safari >= 5',
                    'Opera >= 10',
                    'Explorer >= 9'
                ]
            },
            dist: {
                src: '<%= config.tmp %>/css/*.css'
            }
        },

        // cache-buster
        rev: {
            dist: [
                '<%= config.tmp %>/js/*.js',
                '<%= config.tmp %>/css/*.css'
            ]
        },

        useminPrepare: {
            index: {
                src: ['<%= config.src %>/index.html']
            },
            options: {
                dest: '<%= config.tmp %>'
            }
        },

        usemin: {
            html: ['<%= config.tmp %>/index.html'],
            css: ['<%= config.tmp %>/*.css'],
            options: {
                dirs: ['<%= config.tmp %>/js', '<%= config.tmp %>/css']
            }
        },

        // compress the HTML
        htmlmin: {
            options: {
                removeComments: true,
                removeCommentsFromCDATA: true,
                collapseWhitespace: true,
                collapseBooleanAttributes: true,
                removeAttributeQuotes: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeOptionalTags: true
            },
            views: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.src %>',
                        src: ['views/**/*.tpl.html'],
                        dest: '<%= config.tmp %>'
                    }
                ]
            },
            index: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.dist %>',
                        src: ['index.html'],
                        dest: '<%= config.dist %>'
                    }
                ]
            }
        },

        // annotate dependency injections for minification
        ngAnnotate: {
            options : {
                singleQuotes: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.tmp %>/concat/js',
                        src: ['**/scripts.js'],
                        dest: '<%= config.tmp %>/concat/js',
                    }
                ]
            }
        },

        // convert html templates into javascript
        html2js: {
            options: {
                module: 'xdata.templates',
                quoteChar: '\'',
                useStrict: true
            },
            tmp: {
                src: '<%= config.src %>/views/**/*.tpl.html',
                dest: '<%= config.tmp %>/js/partials.js'
            }
        },

        // js/css uglify
        uglify: {
            options: {
                mangle: false
            }
        },

        // builtin webserver with proxy
        connect: {
            options: {
                hostname: '0.0.0.0',
                port: 9595
            },
            dev: {
                options: {
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            connect.static(require('path').resolve(config.src)),
                            require('grunt-connect-proxy/lib/utils').proxyRequest
                        ];
                    }
                },
                proxies: [{
                    context: '/api',
                    port: '<%= config.apiPort %>',
                    host: '<%= config.api %>',
                    changeOrigin: true
                }]
            },
            dist: {
                options: {
                    keepalive:true,
                    middleware: function (connect) {
                        return [
                            lrSnippet,
                            connect.static(require('path').resolve(config.dist)),
                            require('grunt-connect-proxy/lib/utils').proxyRequest
                        ];
                    }
                },
                proxies: [{
                    context: '/api',
                    port: '<%= config.apiPort %>',
                    host: '<%= config.api %>',
                    changeOrigin: true
                }]
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc',
                ignores: []
            },
            all: [
                'Gruntfile.js',
                '<%= config.src %>/<%= config.appJs %>'
            ]
        },

        watch: {
            less: {
                files: ['<%= config.src %>/css/**/*.less'],
                tasks: ['less:dev']
            },
            index: {
                options: {
                    livereload: liveReloadEnabled  && config.liveReloadPort
                },
                files: ['<%= config.src %>/index.tpl.html'],
                tasks: ['index']
            },
            js: {
                options: {
                    livereload: liveReloadEnabled && config.liveReloadPort
                },
                files: [
                    '<%= config.src %>/<%= config.appJs %>',
                ],
                tasks: ['jshint']
            }
        },

        // concat the partials to the main app file
        // so it can be included in the usemin process
        concat: {
            partials: {
                src: ['<%= config.tmp %>/js/xdata.js', '<%= config.tmp %>/js/partials.js'],
                dest: '<%= config.tmp %>/js/xdata.js'
            }
        }
    });

    function fixPaths(paths) {
        // remove path prefix (src) from file patterns
        var dirRE = new RegExp( '^(' + grunt.config('config.src') + ')\/', 'g' );
        return paths.map(function(file) {
            return file.replace(dirRE, '');
        });
    }

    // Generate the index.html from template
    grunt.registerTask('index', 'Generate index.html', function() {
        var config = grunt.config('config'),
            dst = config.src + '/index.html',
            src = config.src + '/index.tpl.html',
            tpl = grunt.file.read(src);

        // ensure our app bootstrap is the first file loaded
        // nb: can be resolved if each file decalres its own module
        var entryPoint = config.src + '/' + config.mainJs,
            allAppJs = [entryPoint];
        // add other app files
        // remove the main entry point
        // and the specs :)
        allAppJs = allAppJs.concat(grunt.file.expand([
            config.src + '/' + config.appJs,
            '!' + entryPoint,
            '!' + config.src + '/' + config.appSpecs
        ]));

        grunt.file.write(dst, grunt.template.process(tpl, {
            data: {
                bowerCss: fixPaths(grunt.file.expand(config.bowerCss)),
                bowerJs: fixPaths(grunt.file.expand(config.bowerJs)),
                vendorsJs: fixPaths(grunt.file.expand(config.vendorsJs)),
                appJs: fixPaths(allAppJs),
            }
        }));

        grunt.log.writeln('Generated \'' + dst + '\' from \'' + src + '\'');
    });

    grunt.registerTask('dev', 'dev environnement', [
        'index',
        'less:dev',
        'configureProxies:dev',
        'connect:dev',
        'watch'
    ]);

    grunt.registerTask('jshint', 'launch unit tests', [
        'jshint'
    ]);

    grunt.registerTask('dist', 'build distribution and run test server', [
        'clean',
        // prepare the finale css
        'less:tmp',
        'autoprefixer',
        // generate the index
        'index',
        // build the partials in $templateCache
        'html2js:tmp',
        // minify all the scripts
        'useminPrepare',
        'copy:tmp',
        'htmlmin:views',
        'concat:partials',
        'concat:generated',
        // concat and minify the css files
        'cssmin',
        // di notation
        'ngAnnotate',
        'uglify',
        // revision file names (cache buster)
        'rev',
        'usemin',
        // copy final files to the dist folder
        'copy:dist',
        'copy:fonts',
        // cleanup
        'clean:tmp'
    ]);

    grunt.registerTask('default', ['dev']);

    grunt.registerTask('testdist', 'test in dist environement', [
        'configureProxies:dist',
        'connect:dist'
    ]);

};
