(function () {
    'use strict';

    var module = angular.module('xdata.api', []);
    module.factory('xdataApi', function ($http, $filter, $q) {
        var dateFilter = $filter('date');
        return {
            zones: function (zoneId) {
                return $http.get('/api/geo/'+zoneId).then(function(data) { return data.data; });
            },
            mapDay: function (date) {
                var dateString = dateFilter(date, 'yyyy-MM-dd');
                return $http.get('/api/map/day/'+dateString).then(function(data) { return data.data; });
            },
            range: function() {
                var begin = new Date('2012-11-03');
                var end = new Date('2013-08-31');
                var result = [];
                while(begin.getTime() <= end.getTime()) {
                    result.push(begin);
                    begin = new Date(begin);
                    begin.setDate(begin.getDate()+1);
                }
                var defer = $q.defer();
                defer.resolve(result);
                return defer.promise;
            },
            city: function (city) {
                return $http.get('/api/map/city/'+city).then(function(data) { return data.data; });
            }
        };
    });
})();