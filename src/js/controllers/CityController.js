(function () {
    'use strict';

    angular.module('xdata').controller('CityController', function ($scope, xdataApi, $filter) {
        $scope.characteristicConfig = {
            options: {
                chart: {
                    type: 'line'
                },
                legend: {
                    enabled: false
                }
            },
            title: {
                text: null
            },
            yAxis: {
                labels: { enabled: false},
                title: {text: null, enabled: false},
                minRange: 0.2
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%B',
                    month: '%B'
                }
            },
            series: [
                {
                    pointInterval: 24 * 3600 * 1000,
                    data: [],
                    name: "Caractéristique"
                }
            ]
        };
        $scope.balanceConfig = {

            options: {
                chart: {
                    type: 'column'
                },

                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
                },
                legend: {
                    enabled: false
                }
            },

            //The below properties are watched separately for changes.
            series: [
                {data: [], name: "Equilibre des activitées"}
            ],
            title: {
                text: null
            },
            yAxis: {
                title: {text: null, enabled: false},
                minRange: 10
            },
            xAxis: {
                categories: []
            }
        };

        $scope.gaugeConfig = {
            options: {
                chart: {
                    type: 'solidgauge'
                },

                title: null,

                pane: {
                    center: ['50%', '100%'],
                    size: '200%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            },
            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0,
                labels: {
                    enabled: false
                },
                min: 0,
                max: 100,
                title: {
                    enabled: true
                }
            },
            series: [
                {
                    name: 'Activité',
                    data: [0],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:22px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><span style="font-size:12px;color:silver;"> / 100</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' km/h'
                    }
                }
            ]
        };
        var metricBak = $scope.select.metric;
        $scope.toggleSimilar = function(drawSimilar) {
            if(drawSimilar) {
                metricBak = $scope.select.metric;
                $scope.select.metric = 6;
            } else {
                $scope.select.metric = metricBak;
            }
        };

        $scope.clusterLabel = function () {
            var clusters = $scope.data.clusters;
            var arr = [];
            if (clusters[3] === 3)
                arr.push("Saisonal hiver");
            if (clusters[3] === 0)
                arr.push("Saisonal été");
            if (clusters[4] === 14)
                arr.push("Grande agglomération");
            if (clusters[4] === 2)
                arr.push("Petite agglomération");
            if (arr.length === 0)
                return "Campagne";
            return arr.join(", ")
        };
        var translateMetricFr = {
            "sms": "SMS",
            "calls": "Appels",
            "electricity": "Electricité"
        };
        $scope.$watch("select.city", function (city) {
            if (!city) {
                $scope.select.cluster = 0;
                return;
            }
            xdataApi.city(city).then(function (data) {
                $scope.data = data;
                $scope.select.cluster = data.clusters[4];
                var metrics = Object.keys(data.data[0]);
                metrics = metrics.filter(function (metric) {
                    return metric !== "date" && metric !== "total";
                });
                var metricLength = metrics.length;
                var cumulative = {};
                var caracteristic = [];
                var sum = 0;
                for (var j = 0; j < data.data.length; j++) {
                    var d = data.data[j];
                    caracteristic.push(d["total"]);
                    sum += d["total"];
                    for (var i = 0; i < metricLength; i++) {
                        var m = metrics[i];
                        cumulative[m] = (cumulative[m] || 0) + d[m];
                    }
                }
                var total = 0;
                for (var i = 0; i < metricLength; i++) {
                    total += cumulative[metrics[i]];
                }
                $scope.characteristicConfig.series[0].data = caracteristic;
                $scope.characteristicConfig.series[0].pointStart = new Date(data.data[0]["date"]).getTime();
                $scope.balanceConfig.xAxis.categories = metrics.map(function (m) {
                    return translateMetricFr[m];
                });
                $scope.gaugeConfig.series[0].data[0] = Math.round(sum * 100 / data.data.length);
                $scope.balanceConfig.series[0].data = metrics.map(function (m) {
                    return (cumulative[m] / total - 1 / metricLength) * 100;
                });
            });
        });
    });
})();