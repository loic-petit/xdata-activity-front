(function () {
    'use strict';

    angular.module('xdata').controller('MapController', function ($scope, xdataApi, $filter, $timeout) {
        var dateFilter = $filter('date');
        $scope.select = {
            date: null,
            rangeSelect: 0,
            metric: 0,
            cluster: 0
        };
        $scope.validateRangeSelection = function () {
            $scope.select.date = $scope.range[$scope.select.rangeSelect];
        };
        xdataApi.range().then(function (range) {
            $scope.range = range;
            $scope.select.date = range[$scope.select.rangeSelect];
        });
        $scope.alive = null;
        $scope.start = function () {
            if ($scope.alive) {
                return;
            }
            $scope.alive = setInterval(function () {
                $scope.select.rangeSelect = ($scope.select.rangeSelect + 7) % $scope.range.length;
                $scope.select.date = $scope.range[$scope.select.rangeSelect];
                $scope.$digest();
            }, 1500);
        };
        $scope.stop = function () {
            if (!$scope.alive) {
                return;
            }
            clearInterval($scope.alive);
            $scope.alive = null;
        };
        $scope.toggle = function () {
            if ($scope.alive) {
                $scope.stop();
            } else {
                $scope.start();
            }
        };

        $scope.redrawMap = function () {
            var metric = $scope.select.metric;
            var scale = $scope.scaleFactory(metric);
            $scope.colorMap = function (id) {
                var value = $scope.data[id];
                //var value = Math.random();
                if (typeof value === 'undefined')
                    return '#ddd';
                return scale(value[metric], id);
            };
            // let the scope propagation do its magic before sending the event that the color map changed
            $timeout(function () {
                $scope.$broadcast('colorMapChanged')
            });
        };

        $scope.dateFormatting = function (idx) {
            return dateFilter($scope.range[idx], 'dd-MM-yyyy');
        };

        $scope.$watchCollection('[select.metric, select.cluster, select.city]', function (nv, ov) {
            if(!ov) return;
            $scope.redrawMap();
        });

        var qualitativeColors = ["#e41a1c", "#377eb8"];
        var absoluteScale = chroma.scale(['white', 'red']).domain([0.5, 1.1]);
        var lowerScale = chroma.scale(['white', 'blue']).domain([0, 0.3]);
        var higherScale = chroma.scale(['white', 'red']).domain([0, 0.2]);
        var relativeScale = function (value) {
            if (value < 0) return lowerScale(-value);
            return higherScale(value);
        };

        $scope.scaleFactory = function (metric) {
            if (metric === 0) {
                return function (value) {
                    return absoluteScale(value).hex();
                }
            } else if (metric === 1) {
                return function (value) {
                    return relativeScale(value).hex();
                }
            } else if (metric === 6) {
                return function (value, id) {
                    if (id === $scope.select.city)
                        return qualitativeColors[0];
                    else if (value === $scope.select.cluster)
                        return qualitativeColors[1];
                    else
                        return '#eee';
                }
            }
            return function (value) {
                return qualitativeColors[value % qualitativeColors.length];
            }
        };

        $scope.$watch("select.date", function (date) {
            if (!date)
                return;
            xdataApi.mapDay(date).then(function (data) {
                $scope.data = data;
                $scope.redrawMap();
            });
        });
    });
})();
