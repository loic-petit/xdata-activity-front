/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */

(function () {
    'use strict';

    // this module will be replaced in dist with the pre-cached templates
    angular.module('xdata.templates', []);

    angular.module('xdata', [
        'ui.router',
        'ui.bootstrap',
        'xdata.canvasmap',
        'vr.directives.slider',
        'highcharts-ng'
    ]).config(function ($locationProvider, $stateProvider, $urlRouterProvider) {
        $locationProvider.html5Mode(false);
        Highcharts.setOptions({
            lang: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',  'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
            }
        });
        $stateProvider
            .state('home', {
                url: '/home/',
                views: {
                    'main@': {
                        templateUrl: 'views/home.tpl.html',
                        controller: 'MapController'
                    }
                }
            });
        $urlRouterProvider.otherwise('/home/');
    }).controller('RootController', function ($scope) {
        $scope.loading = false;
    });
})();
