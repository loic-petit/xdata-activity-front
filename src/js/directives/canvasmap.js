(function () {
    'use strict';

    function prepare(m) {
        m.centerX = parseFloat(m.centerX);
        m.centerY = parseFloat(m.centerY);
        var bb = {
            x: null,
            y: null
        };
        var split = m.path.trim().split(' ');
        for (var i = 0, l = split.length; i < l; i++) {
            var c = split[i];
            var x, y;
            if (c === 'M') {
                x = parseFloat(split[i + 1]);
                y = parseFloat(split[i + 2]);
                split[i + 1] = x;
                split[i + 2] = y;
                i += 2;
            } else if (c !== 'L' && c !== 'Z') {
                x = parseFloat(split[i]);
                y = parseFloat(split[i + 1]);
                split[i] = x;
                split[i + 1] = y;
                i += 1;
            } else {
                continue;
            }

            if (bb.x == null) {
                bb.x = [x, x];
                bb.y = [y, y];
            } else {
                if (bb.x[0] > x)
                    bb.x[0] = x;
                if (bb.x[1] < x)
                    bb.x[1] = x;
                if (bb.y[0] > y)
                    bb.y[0] = y;
                if (bb.y[1] < y)
                    bb.y[1] = y;
            }
        }
        m.preparedPath = split;
        return bb;
    }

    function computeBB(zones) {
        var globalBB = {x: null, y: null};
        zones.forEach(function (zone) {
            var bb = prepare(zone);
            if (globalBB.x == null) {
                globalBB.x = bb.x;
                globalBB.y = bb.y;
            } else {
                if (globalBB.x[0] > bb.x[0])
                    globalBB.x[0] = bb.x[0];
                if (globalBB.x[1] < bb.x[1])
                    globalBB.x[1] = bb.x[1];
                if (globalBB.y[0] > bb.y[0])
                    globalBB.y[0] = bb.y[0];
                if (globalBB.y[1] < bb.y[1])
                    globalBB.y[1] = bb.y[1];
            }
        });
        return {
            centerX: (globalBB.x[0] + globalBB.x[1]) / 2,
            centerY: (globalBB.y[0] + globalBB.y[1]) / 2,
            width: globalBB.x[1] - globalBB.x[0],
            height: globalBB.y[1] - globalBB.y[0]
        };
    }

    var module = angular.module('xdata.canvasmap', ['xdata.api']);
    module.directive('canvasmap', function (xdataApi) {
        return {
            restrict: 'E',
            scope: {
                data: '=',
                backgroundZones: '@',
                ngModel: '=',
                zones: '@',
                metric: '&',
                colorFn: '='
            },
            template: '<canvas></canvas>',
            link: function (scope, element) {
                scope.canvasElt = $(element).find("canvas")[0];
                scope.width = $(element).innerWidth();
                scope.height = $(element).innerHeight()-3;
                scope.canvasElt.width = scope.width;
                scope.canvasElt.height = scope.height;
                xdataApi.zones(scope.backgroundZones).then(function (zones) {
                    scope.backgroundZones = zones;
                    scope.backgroundZonesBB = computeBB(zones);
                    xdataApi.zones(scope.zones).then(function (zones) {
                        scope.zones = zones;
                        scope.zonesBB = computeBB(zones);
                        scope.draw();
                        scope.$on('colorMapChanged', scope.draw);
                    });
                });

                var drag = null;
                scope.canvasElt.onmousedown = function (event) {
                    drag = {
                        x: event.offsetX,
                        y: event.offsetY,
                        refresh: setInterval(scope.render, 16),
                        hasMoved: false
                    };
                    scope.canvasElt.onmousemove = function (event) {
                        scope.camera.x += (event.movementX);
                        scope.camera.y += (event.movementY);
                        drag.hasMoved = true
                    };
                };
                scope.canvasElt.onmouseup = function (event) {
                    clearInterval(drag.refresh);
                    if (!drag.hasMoved) {
                        scope.$apply(function () {
                            scope.ngModel = scope.getZoneId(drag.x, drag.y);
                        });
                    }
                    scope.canvasElt.onmousemove = null;
                    drag = null;
                };
                var renderTimeout = null;
                $(scope.canvasElt).mousewheel(function (event) {
                    if (event.deltaY > 0) {
                        scope.camera.zoom *= 1.1;
                        scope.camera.x = scope.camera.x * 1.1 - event.offsetX * 0.1;
                        scope.camera.y = scope.camera.y * 1.1 - event.offsetY * 0.1;
                    } else {
                        scope.camera.zoom /= 1.1;
                        scope.camera.x = scope.camera.x / 1.1 + event.offsetX * (0.1 / 1.1);
                        scope.camera.y = scope.camera.y / 1.1 + event.offsetY * (0.1 / 1.1);
                    }
                    scope.render();
                    event.preventDefault();
                    if (renderTimeout) {
                        clearTimeout(renderTimeout);
                    }
                    renderTimeout = setTimeout(function () {
                        renderTimeout = null;
                        scope.draw();
                    }, 300);
                });
            },
            controller: function ($scope) {
                $scope.camera = {
                    x: 0,
                    y: 0,
                    zoom: 10
                };
                function drawCanvas(zones, zonesBB, zoom, value, strokeStyle) {
                    var t = new Date().getTime();
                    var canvas = document.createElement('canvas');
                    canvas.width = zonesBB.width * zoom;
                    canvas.height = zonesBB.height * zoom;
                    var c = canvas.getContext('2d');
                    c.strokeStyle = strokeStyle;
                    var z = zones;
                    var canvasCenterX = c.canvas.width / 2;
                    var canvasCenterY = c.canvas.height / 2;
                    var cameraX = zonesBB.centerX;
                    var cameraY = zonesBB.centerY;
                    var cameraRatio = zoom;
                    c.save();
                    for (var j = 0, lz = z.length; j < lz; ++j) {
                        var zone = z[j];
                        var path = zone.preparedPath;
                        c.save();
                        c.beginPath();
                        c.fillStyle = value(zone.id);
                        for (var i = 0, l = path.length; i < l; i++) {
                            var m = path[i];
                            if (m === 'M') {
                                c.moveTo((path[++i] - cameraX) * cameraRatio + canvasCenterX,
                                        (path[++i] - cameraY) * cameraRatio + canvasCenterY);
                            } else if (m !== 'L') {
                                if (m !== 'Z') {
                                    c.lineTo((m - cameraX) * cameraRatio + canvasCenterX,
                                            (path[++i] - cameraY) * cameraRatio + canvasCenterY);
                                } else {
                                    c.closePath();
                                }
                            }
                        }
                        if (strokeStyle)
                            c.stroke();
                        c.fill();
                        c.restore();
                    }
                    c.restore();
                    t = new Date().getTime() - t;
                    console.log('Canvas rendered in ' + t + 'ms');
                    return canvas;
                }

                $scope.getZoneId = function (canvasX, canvasY) {
                    var originX = canvasX - $scope.camera.x;
                    var originY = canvasY - $scope.camera.y;
                    var x = originX * 20 / $scope.camera.zoom;
                    var y = originY * 20 / $scope.camera.zoom;
                    var color = $scope.eventMap.getContext('2d').getImageData(x, y, 1, 1).data;
                    var id = ((color[0] << 16) + (color[1] << 8) + color[2]).toString();
                    if (id == 0)
                        return null;
                    while (id.length < 5)
                        id = "0" + id;
                    if (id.length > 5)
                        return null;
                    return id;
                };
                var backgroundFilter = {
                    '18': 1,
                    '26': 1,
                    '28': 1,
                    '36': 1,
                    '37': 1,
                    '38': 1,
                    '41': 1,
                    '42': 1,
                    '44': 1,
                    '45': 1,
                    '49': 1,
                    '53': 1,
                    '69': 1,
                    '72': 1,
                    '73': 1,
                    '74': 1,
                    '85': 1,
                    '07': 1,
                    '01': 1
                };
                var backgroundMapColor = "#ddd";
                var backgroundColor = "#fdfdfd";
                $scope.draw = function () {
                    if (!$scope.eventMap) {
                        $scope.eventMap = drawCanvas($scope.zones, $scope.zonesBB, 20, function (id) {
                            // ooh I like that
                            var hexId = parseInt(id).toString(16);
                            while (hexId.length < 6)
                                hexId = "0" + hexId;
                            return "#" + hexId;
                        }, null);
                    }
                    $scope.canvas = drawCanvas($scope.zones, $scope.zonesBB, $scope.camera.zoom, $scope.colorFn, $scope.camera.zoom > 12 ? '#aaa' : null);
                    $scope.backgroundCanvas = drawCanvas($scope.backgroundZones, $scope.backgroundZonesBB, $scope.camera.zoom, function (id) {
                        if (!id) return null;
                        id = id.substring(0, 2);
                        if (backgroundFilter[id])
                            return "rgba(0,0,0,0)";
                        else
                            return backgroundMapColor;
                    }, '#aaa');
                    $scope.backgroundOffset = {
                        x: $scope.backgroundZonesBB.centerX - $scope.zonesBB.centerX - ($scope.backgroundZonesBB.width - $scope.zonesBB.width) / 2,
                        y: $scope.backgroundZonesBB.centerY - $scope.zonesBB.centerY - ($scope.backgroundZonesBB.height - $scope.zonesBB.height) / 2
                    };

                    $scope.render();
                };
                $scope.render = function () {
                    var canvas = $scope.canvasElt;
                    var c = canvas.getContext('2d');
                    c.fillStyle = backgroundColor;
                    c.rect(0, 0, $scope.width, $scope.height);
                    c.fill();
                    c.beginPath();
                    c.drawImage($scope.canvas, $scope.camera.x, $scope.camera.y, $scope.zonesBB.width * $scope.camera.zoom, $scope.zonesBB.height * $scope.camera.zoom);
                    c.drawImage($scope.backgroundCanvas, $scope.camera.x + $scope.backgroundOffset.x * $scope.camera.zoom, $scope.camera.y + $scope.backgroundOffset.y * $scope.camera.zoom, $scope.backgroundZonesBB.width * $scope.camera.zoom, $scope.backgroundZonesBB.height * $scope.camera.zoom);

                };
            }
        };
    });
})();