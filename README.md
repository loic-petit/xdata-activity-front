##USAGE

### Have the standard tools
Have node with npm installed.

You must have bower and grunt-cli packages available locally.

### Init

```
bower install
npm install
```

### Grunt

 - `grunt` : run the dev server, proxy to API, watch your changes
 - `grunt dist` : full build for distribution

#### Grunt switches
 - `--livereload` : enable livereload

