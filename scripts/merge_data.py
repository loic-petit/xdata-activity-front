import json

__author__ = 'Loic Petit'


orange = json.load(open("resources/orange.json"))
edf = json.load(open("resources/edf.json"))
cities = json.load(open("resources/cities.json"))
days = json.load(open("resources/days.json"))

print("Read data")

merge = [orange, edf]

final = dict()
for day in days:
    by_day = dict()
    final[day] = by_day
    for insee in cities:
        values = {}
        for data in merge:
            data_by_day = data.get(day)
            if data_by_day is None:
                continue
            data_values = data_by_day.get(insee)
            if data_values is None:
                continue
            for metric, value in data_values.items():
                values[metric] = value
        by_day[insee] = values

json.dump(final, open("resources/final.json", "w"))