import csv
import json
import math

__author__ = 'Loic Petit'
# for row in csv.reader(open("query/quantmetry/labels_clusters.csv"), delimiter=';'):
# no, cluster, lat, lon, labels_cluster_15, labels_cluster_2, labels_cluster_3, labels_cluster_4, labels_cluster_7 = row
#
# if no == "":
#         continue
#
orange = json.load(open("resources/final.json"))
days = json.load(open("resources/days.json"))
cities = json.load(open("resources/cities.json"))
cluster_map = json.load(open("../data/cluster-map.json"))
cluster_data = json.load(open("../data/cluster-data.json"))
print("Loaded")


def pre_compute(value):
    return math.log(1 + value)

metrics = {"calls", "sms", "electricity"}


def stabilize(input_data):
    global metrics, days, cities
    caracteristic = dict()

    total = dict()
    for m in metrics:
        total[m] = None

    for day in days:
        per_day = dict()
        for m in metrics:
            per_day[m] = []
        for insee in cities:
            values = input_data[day][insee]
            for m in metrics:
                value = values.get(m)
                if value is not None:
                    per_day[m].append(value)
        # Compute avg
        per_day = {m: sorted(per_day[m])[int(len(per_day[m])/2)] for m in metrics}
        caracteristic[day] = per_day
        for m in metrics:
            if total[m] is None or total[m] < per_day[m]:
                total[m] = per_day[m]

    for day in days:
        for insee in cities:
            values = input_data[day][insee]
            for m in metrics:
                value = values.get(m)
                if value is not None:
                    values[m] = value / caracteristic[day][m] * total[m]

def normalize(input_data):
    global metrics
    minmax = dict()
    for data in input_data.values():
        for dp in data.values():
            for m in metrics:
                if m not in dp:
                    continue
                value = pre_compute(dp[m])
                if m not in minmax:
                    minmax[m] = [value, value]
                    continue
                bounds = minmax[m]
                if value < bounds[0]:
                    bounds[0] = value
                if value > bounds[1]:
                    bounds[1] = value

    return {
        d: {
            c: {
                m: (pre_compute(value) - minmax[m][0]) / (minmax[m][1] - minmax[m][0])
                for m, value in dp.items()
            }
            for c, dp in data.items()
        }
        for d, data in input_data.items()
    }

stabilize(orange)
orange = normalize(orange)
print("Normalized")


def compute_global_metric(data):
    # Linear computing
    data = {c: sum([value / len(dp) for value in dp.values()]) for c, dp in data.items()}
    # exit(1)
    return data

per_cities = dict()
for city in cities:
    per_cities[city] = []

per_days = dict()
for day in days:
    data = orange[day]
    agregated = compute_global_metric(data)
    for city, dp in data.items():
        dp["total"] = agregated[city]
        dp["date"] = day
        per_cities[city].append(dp)
    per_days[day] = agregated

minmax_by_city = dict()
for city, values in per_cities.items():
    data = sorted([v["total"] for v in values])
    minmax_by_city[city] = data[int(len(data)/2)]
    json.dump(values, open("../data/map/city/" + city + ".json", "w"))
print("Per cities computed")


for day in days:
    agregated = per_days[day]
    for city in cities:
        agregated[city] = [agregated[city], agregated[city]-minmax_by_city[city]] + cluster_data[cluster_map[city]]

    json.dump(agregated, open("../data/map/day/" + day + ".json", "w"))
print("Per day stats computed")