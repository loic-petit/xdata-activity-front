import csv

first = True
for i, row in enumerate(csv.reader(open("query_result.csv"))):
    if i == 0:
        continue
    print("SELECT '%s' as lat, '%s' as lon, codeinsee FROM commune where ST_Within(ST_SetSRID(ST_MakePoint(%s,%s),4326), ST_Transform(geometry, 4326));" % (row[0], row[1], row[1], row[0]))