import json

import kdtree
from scripts.node import Node



# def distance(centerX, centerY):
#     for target in gis:
#         dist = math.sqrt((centerX - target["centerX"]) ** 2 + (centerY - target["centerY"]) ** 2)
#         if dist == 0:
#             continue
#         yield [target["id"], dist]



gis = json.load(open("resources/gis.json"))


tree = kdtree.create(dimensions=2)

result = dict()
for com in gis:
    tree.add(Node(com["centerX"], com["centerY"], com["id"]))

for com in gis:
    res = [[n[0].data.data, n[1]] for n in tree.search_knn((com["centerX"], com["centerY"]), 70) if n[1] > 0]
    result[com["id"]] = res

json.dump(result, open("resources/distance.json", "w"))
