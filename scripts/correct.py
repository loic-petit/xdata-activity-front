import json


def align_geo(data, distance):
    share_list = dict()
    result = dict()
    for gis, distance_list in distance.items():
        result[gis] = {"sms": 0, "calls": 0}
        if gis in data:
            if gis not in share_list:
                share_list[gis] = dict()
            share_list[gis][gis] = 1
            continue
        found = None
        total = []
        for distance_data in distance_list:
            if distance_data[0] in data:
                if found is None:
                    found = distance_data[1] + 0.25
                    total.append(distance_data[0])
                elif distance_data[1] < found:
                    total.append(distance_data[0])

        if found is None:
            print("You suck! " + gis)
            exit(1)

        for insee in total:
            if insee not in share_list:
                share_list[insee] = dict()
            share_list[insee][gis] = 1 / len(total)
    # print(share_list)

    def merge(dp, base, ratio):
        for k in dp:
            dp[k] += base[k] * ratio

    for insee, share in share_list.items():
        total_load = sum([i for i in share.values()])
        base = data[insee]
        for target, ratio in share.items():
            merge(result[target], base, ratio / total_load)
    return result


orange_data = json.load(open("resources/orange_raw.json"))
distance = json.load(open("resources/distance.json"))
orange_corrected = {day: align_geo(data, distance) for day, data in orange_data.items()}
json.dump(orange_corrected, open("resources/orange.json", "w"))
