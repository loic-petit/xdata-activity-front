__author__ = 'Loic Petit'


class Node:
    def __init__(self, x, y, data):
        self.x = x
        self.y = y
        self.data = data

    def __len__(self):
        return 2

    def __getitem__(self, item):
        if item == 0:
            return self.x
        elif item == 1:
            return self.y
        elif item == "data":
            return self.data
        raise Exception("Bad Item")

    def __iter__(self):
        yield self.x
        yield self.y

