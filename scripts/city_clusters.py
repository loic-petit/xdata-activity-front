import csv, kdtree
import json
import operator

import math

# from http://www.johndcook.com/python_longitude_latitude.html
from scripts.node import Node


def distance_on_unit_sphere(N1, N2):
    lat1 = N1[0]
    long1 = N1[1]
    lat2 = N2[0]
    long2 = N2[1]

    # Convert latitude and longitude to
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0

    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians

    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians

    # Compute spherical distance from spherical coordinates.

    # For two locations in spherical coordinates
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) =
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length

    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth
    # in your favorite set of units to get length.
    return arc * 6373

clusters = [[]] * 500
idx_init = []
for row in csv.reader(open("query/quantmetry/labels_clusters.csv"), delimiter=';'):
    no, cluster, lat, lon, labels_cluster_15, labels_cluster_2, labels_cluster_3, labels_cluster_4, labels_cluster_7 = row

    if no == "":
        continue
    cluster = int(cluster)
    clusters[cluster] = (int(labels_cluster_2), int(labels_cluster_3), int(labels_cluster_4), int(labels_cluster_7), int(labels_cluster_15))
    idx_init.append(Node(float(lat), float(lon), cluster))

print("Finished parsing data")
idx = kdtree.create(point_list=idx_init, dimensions=2)
print("Finished adding to idx")

centroids = dict()
for row in csv.reader(open("query/centroids.csv")):
    insee, lon, lat = row
    if lon == "":
        continue
    centroids[insee] = (float(lat), float(lon))

selected_clusters = dict()
i = 0
for gis in json.load(open("resources/gis.json")):
    coords = centroids[gis["id"]]
    # print(str(i)+" / "+str(len(clusters_around)))
    selected_clusters[gis["id"]] = idx.search_nn(coords)[0].data.data # get the first cluster
json.dump(selected_clusters, open("../data/cluster-map.json", "w"))
json.dump(clusters, open("../data/cluster-data.json", "w"))

