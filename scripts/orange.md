### Orange Import

All the data has been dumped to the HCat table orange_total using one partition for each csv.

## Antennas extract

To extract the list of antennas provided by Orange. Execute the following pig script
```
data = LOAD 'orange_total' USING org.apache.hcatalog.pig.HCatLoader();
grouped = GROUP data BY (lat, lon);
result = FOREACH grouped GENERATE group.lat as lat, group.lon as lon, COUNT(data) as c;
STORE result INTO 'orange_extracted_points' USING org.apache.hcatalog.pig.HCatStorer();
```

Make sure the result table is empty.

## Antennas alignment
Execute the ```pg_search_gis``` script with the extracted csv to generate the queries to execute on the Geo database from DataPublica.

Import the result in an HCat table ```orange_antennas```.

This is working only on datapublica's bases... We further consider the ```orange_antennas``` table as a input. At this stade, we have two tables in hive : ```orange_antennas``` and ```orange_cra```. 

## Extract the data by day by city
```
INSERT OVERWRITE TABLE orange_Insee_Jour
SELECT 
	substring(q.t, 0, 10) as d,
 	a.insee, 
 	sum(q.nb_voix_entrant) as nb_voix_entrant, 
 	sum(q.nb_voix_sortant) as nb_voix_sortant, 
 	sum(q.nb_sms_entrant) as nb_sms_entrant, 
 	sum(q.nb_sms_sortant) as nb_sms_sortant,
 	count(distinct q.mnc) as distinct_mnc
FROM orange_antennas as a 
JOIN orange_cra as q 
	ON (a.lat = q.lat AND a.lon = q.lon) 
GROUP BY substring(q.t, 0, 10), a.insee;
```

Download the result table. (result in result.csv)

## Generate GIS paths
Execute the following query to extract gis (centerX/centerY/path) from geo. (result com.csv)

!! Need explanations !!

```
COPY (SELECT codeinsee as insee, ST_asSVG(ST_Scale(ST_Transform(z.geometry,900913),0.0001,0.0001),0,3) as path,  ST_X(ST_Centroid(ST_Scale(ST_Transform(z.geometry,900913),0.0001,0.0001))) as centerX, ST_Y(ST_Centroid(ST_Scale(ST_Transform(z.geometry,900913),0.0001,0.0001))) as centerX FROM Commune z) to STDOUT with csv;
```

## Generate the gis.json
Run the merge_orange.py.

This json contains all the cities used for display and can be given to the js application.

## Generate the distance map
Run the distance.py.

This json contains the list of top-50 cities close to each other. It is needed for data correction.

## Generate the orange final
Run the correct.py

This corrects data by spreading the load of each unknown city to the closest antennas.

## Finalize the export
Run the export.py.