__author__ = 'Loic Petit'

import csv
import json

result = dict()
first = True
inseecodes = set()
for row in csv.reader(open("query/IRISJournalier.csv"), delimiter=';'):
    if first:
        first = False
        continue
    day, iris, value = row
    if day < "2012-11":
        # cheating alignment to have everything mapped to orange data 2013
        day = day[:3]+'3'+day[4:]
    value = float(value)*1000
    iris = iris[:5]
    inseecodes.add(iris)
    if day not in result:
        result[day] = dict()
    if iris in result[day]:
        result[day][iris]["electricity"] += value
    else:
        result[day][iris] = {"electricity": value}

for code in inseecodes:
    print(code)

print(len(inseecodes))
json.dump(result, open("resources/edf.json", "w"))