__author__ = 'Loic Petit'

import csv, json


data_export = True


gis_idx = dict()
for gis in csv.reader(open("query/com.csv")):
    if gis[1] == "":
        continue
    gis_idx[gis[0]] = gis

result = dict()
final_data = dict()
depts = dict()
days = list()
for line in csv.reader(open("query/result.csv")):
    day, lat, lon, insee, nb_voix_sortant,nb_voix_entrant,nb_sms_entrant,nb_sms_sortant = line
    if insee not in gis_idx:
        continue
    if day not in final_data:
        days.append(day)
        final_data[day] = dict()
    data = final_data[day]
    
    dep = insee[0:2]
    depts[dep] = 1 if dep not in depts else 1 + depts[dep]            
    data[insee] = {
        "calls": int(nb_voix_sortant)+int(nb_voix_entrant),
        "sms": int(nb_sms_entrant) + int(nb_sms_sortant)
    }

print(depts)
total_cities = {i for i in gis_idx.keys() if i[0:2] in depts and depts[i[0:2]] > 5000}
total_cities = total_cities.union(set(result.keys()))
final_gis = [{
    "id": gis_idx[insee][0],
    "centerX": float(gis_idx[insee][2]),
    "centerY": float(gis_idx[insee][3]),
    "path": gis_idx[insee][1],
} for insee in total_cities]
json.dump(days, open("resources/days.json", "w"))
json.dump(final_gis, open("resources/gis.json", "w"))
json.dump(list(total_cities), open("resources/cities.json", "w"))
json.dump(final_data, open("resources/orange_raw.json", "w"))