import csv

__author__ = 'Loic Petit'

import datetime

from api_util import json_body, json_datetime, json_request
from bottle import get, route, run, post, request, HTTPError
import json

cluster_map = json.load(open("../data/cluster-map.json"))
cluster_data = json.load(open("../data/cluster-data.json"))

city_label = {row[0]: row[1] for row in csv.reader(open("../data/communes-label.csv"))}

@get('/api/geo/<geo_id>')
@json_body
def geo_data(geo_id):
    try:
        return json.load(open("../data/geo/"+geo_id+".json"))
    except:
        raise HTTPError(status=404, body=geo_id+" not found")

@get('/api/map/day/<day>')
@json_body
def map_data(day):
    try:
        return json.load(open("../data/map/day/"+day+".json"))
    except:
        raise HTTPError(status=404, body=day+" not found")

@get('/api/map/city/<insee>')
@json_body
def map_data(insee):
    try:
        data = json.load(open("../data/map/city/"+insee+".json"))
        label = city_label[insee]
        cluster = cluster_map[insee]
        return {
            "data": data,
            "label": label,
            "clusters": cluster_data[cluster]
        }
    except:
        raise HTTPError(status=404, body=insee+" not found")


run(host='localhost', port=4545)
