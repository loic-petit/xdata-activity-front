import datetime
import json
from bottle import response, request


def json_body(f):
    def wrapper(*args, **kwargs):
        response.content_type = 'application/json'
        return json.dumps(replace_dates(f(*args, **kwargs)))
    return wrapper


def json_request(f):
    def wrapper(*args, **kwargs):
        body = request.json

        if body is None or not isinstance(body, dict):
            response.status = 400
            return {"error": "unreadable json body (requires the content-type header and a json object as body)"}

        return f(body=body, *args, **kwargs)
    return wrapper


def replace_dates(a):
    if isinstance(a, dict):
        return {k: replace_dates(v) for k, v in a.items()}
    elif isinstance(a, list):
        return [replace_dates(v) for v in a]
    if isinstance(a, datetime.datetime):
        return datetime_to_jsontime(a)
    return a


def json_datetime(jt):
    if jt is None:
        return None
    return datetime.datetime.fromtimestamp(jt/1000, tz=datetime.timezone.utc)


def datetime_to_jsontime(dt):
    if dt is None:
        return None
    return int(dt.timestamp() * 1000)